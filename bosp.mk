
ifdef CONFIG_CONTRIB_TUTORIAL_PLAIN

# Targets provided by this project
.PHONY: rtlib_tutorials clean_rtlib_tutorials

# Add this to the "contrib_testing" target
contrib: rtlib_tutorials
clean_contrib: clean_rtlib_tutorials

MODULE_CONTRIB_USER_RTLIBTUTORIAL=contrib/user/rtlib-tutorials

rtlib_tutorials: external
	@echo
	@echo "==== Building RTLib Tutorials ($(BUILD_TYPE)) ===="
	@[ -d $(MODULE_CONTRIB_USER_RTLIBTUTORIAL)/build/$(BUILD_TYPE) ] || \
		mkdir -p $(MODULE_CONTRIB_USER_RTLIBTUTORIAL)/build/$(BUILD_TYPE) || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_RTLIBTUTORIAL)/build/$(BUILD_TYPE) && \
		CXX=$(CXX) cmake \
		$(CMAKE_COMMON_OPTIONS) ../.. || \
		exit 1
	@cd $(MODULE_CONTRIB_USER_RTLIBTUTORIAL)/build/$(BUILD_TYPE) && \
		make -j$(CPUS) install || \
		exit 1

clean_rtlib_tutorials:
	@echo
	@echo "==== Clean-up RTLib Tutorials ===="
	@[ ! -f $(BUILD_DIR)/usr/bin/bbque-tutorial-plain ] || \
		rm -f $(BUILD_DIR)/etc/bbque/recipes/BbqRTLibTutorial*; \
		rm -f $(BUILD_DIR)/usr/bin/bbque-tutorial-*
	@rm -rf $(MODULE_CONTRIB_USER_RTLIBTUTORIAL)/build
	@echo

else # CONFIG_CONTRIB_TUTORIAL_PLAIN

rtlib_tutorials:
	$(warning contib RTLibTtutorial module disabled by BOSP configuration)
	$(error BOSP compilation failed)

endif # CONFIG_CONTRIB_TUTORIAL_PLAIN

