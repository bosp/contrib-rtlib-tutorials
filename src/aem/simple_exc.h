/*
 * Copyright (C) 2012  Politecnico di Milano
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BBQUE_TUTORIAL_AEM_
#define BBQUE_TUTORIAL_AEM_

#include <list>
#include <thread>

#include <bbque/bbque_exc.h>

#define RECIPE_NAME 	"BbqRTLibTutorialAEM"
#define APP_NAME    	"bbq-tutorial-aem"
#define EXC_NAME    	"aem-exc"

using namespace std;
using bbque::rtlib::BbqueEXC;

class SimpleEXC: public BbqueEXC {

public:

	SimpleEXC(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib):
		BbqueEXC(name, recipe, rtlib) {
	};

	~SimpleEXC() { }

	RTLIB_ExitCode_t onSetup();

	RTLIB_ExitCode_t onRun();

	RTLIB_ExitCode_t onConfigure(uint8_t awm_id);

	RTLIB_ExitCode_t onSuspend();

	RTLIB_ExitCode_t onMonitor();

private:

	list<thread> thrds;

	uint8_t num_thrds;

};

#endif // BBQUE_TUTORIAL_AEM_
